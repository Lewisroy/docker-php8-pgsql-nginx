<?php

namespace App\Repository;

use App\Entity\Eidetic;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Eidetic|null find($id, $lockMode = null, $lockVersion = null)
 * @method Eidetic|null findOneBy(array $criteria, array $orderBy = null)
 * @method Eidetic[]    findAll()
 * @method Eidetic[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EideticRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Eidetic::class);
    }

    // /**
    //  * @return Eidetic[] Returns an array of Eidetic objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('e.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Eidetic
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}

### Getting started

```bash
cd docker/ && docker-compose up --build
```

To access directly from local host the PostgreSQL database container

```bash
psql postgresql://postgres:password@127.0.0.1:15432/dbtest
```

To update composer, go to php-fpm container :

```bash
docker ps #To get container id of php-fpm
docker exec -ti containerid bash
composer update
```

If you want to update your code

```bash
git pull
docker exec -ti containerid bash
composer update
php bin/console do:mi:mi
```

If you want to see the SWAGGER documentation:

http://localhost/api/docs
